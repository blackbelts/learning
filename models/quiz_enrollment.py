from odoo import models, fields, api


class QuizTrailsEnrollment(models.Model):
    _name = 'quiz.enrollment'

    code = fields.Char('Quiz Code')

    lesson = fields.Many2one('lesson.enrollment', string='Enrolled Lesson')
    total_score = fields.Float('Quiz Trail Total Mark')
    seq = fields.Char(string='Trail Sequence', default=lambda self: self.env['ir.sequence'].next_by_code('quiz'),
                      readonly=True)
    time = fields.Integer('Quiz Time in minutes')


class QuizQuestionEnrollment(models.Model):
    _name = 'quiz.question.enrollment'

    code = fields.Char('Quiz Question Code')

    lesson = fields.Many2one('lesson.enrollment', string='Enrolled Lesson')
    mark = fields.Float('Quiz Question Mark')


class AnswersEnrollment(models.Model):
    _name = 'answers.enrollment'

    seq = fields.Char(string='Answer Line Sequence', default=lambda self: self.env['ir.sequence'].next_by_code('trail'),
                      readonly=True)
    question = fields.Many2one('learning.question', string='Questions')
    answer = fields.Boolean('T/F')
