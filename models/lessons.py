from odoo import models, fields, api


class Lessons(models.Model):
    _name = 'learning.lessons'

    name = fields.Char('Lesson Title')
    code = fields.Char('Lesson Code')
    seq = fields.Char(string='Lesson Sequence', default=lambda self: self.env['ir.sequence'].next_by_code('lesson'),
                      readonly=True)
    course = fields.Many2one('learning.courses', string='Course')
    attachment_number = fields.Integer(compute='_compute_attachment_number', string='Number of Attachments')
    auto_start = fields.Boolean(string="Auto Start")
    disc = fields.Char('Course Description')
    objectives = fields.Char('Course Objectives')
    resources_count = fields.Integer(compute='_compute_lessons_resources')
    lesson_resources = fields.One2many('learning.lesson.resources', 'lesson_id', string='Lesson Resources')

    # quizzes_count = fields.Integer(compute='_compute_lessons_quizzes')

    @api.one
    def _compute_attachment_number(self):
        s = self.env['ir.attachment'].search(['&', ('res_id', '=', self.id),
                                              ('res_model', '=', 'learning.lessons')])
        self.attachment_number = len(s)
        # pass

    @api.multi
    def action_get_attachment_tree_view(self):
        action = self.env.ref('base.action_attachment').read()[0]
        action['context'] = {
            'default_res_model': self._name,
            'default_res_id': self.ids[0]
        }
        # action['search_view_id'] = (self.env.ref('ir.attachment').id,)
        action['domain'] = ['&', ('res_model', '=', 'learning.lessons'), ('res_id', '=', self.id)]
        return action

    @api.one
    def _compute_lessons_resources(self):
        for partner in self:
            # operator = 'child_of' if partner.is_company else '='
            partner.resources_count = self.env['learning.lesson.resources'].search_count(
                [('lesson_id', '=', self.id)])

    @api.multi
    def show_lesson_resources(self):
        return {
            'name': ('Resources'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'learning.lesson.resources',  # model name ?yes true ok
            'target': 'current',
            'type': 'ir.actions.act_window',
            'context': {'default_lesson_id': self.id},
            'domain': [('lesson_id', '=', self.id)]
        }

    # @api.one
    # def _compute_lessons_quizzes(self):
    #     for partner in self:
    #         # operator = 'child_of' if partner.is_company else '='
    #         partner.quizzes_count = self.env['learning.quiz'].search_count(
    #             [('lesson_id', '=', self.id)])
    #
    # @api.multi
    # def show_lesson_quizzes(self):
    #     return {
    #         'name': ('Quizzes'),
    #         'view_type': 'form',
    #         'view_mode': 'tree,form',
    #         'res_model': 'learning.quiz',  # model name ?yes true ok
    #         'target': 'current',
    #         'type': 'ir.actions.act_window',
    #         'context': {'default_lesson_id': self.id},
    #         'domain': [('lesson_id', '=', self.id)]
    #     }


class Videos(models.Model):
    _inherit = 'ir.attachment'

    content = fields.Char("Content")


class LessonsResources(models.Model):
    _name = 'learning.lesson.resources'

    name = fields.Char('Resource Name')
    lesson_id = fields.Many2one('learning.lessons')
    resource_type = fields.Selection([('quiz', 'Quiz'), ('url', 'URL'),
                                      ('video', 'Video'), ('file', 'File Attachment')],
                                     'Resource Type', required=True)
    url = fields.Char('URL')
    auto_start = fields.Boolean(string="Auto Start")
    res_seq = fields.Char(string='Resource Sequence',
                          default=lambda self: self.env['ir.sequence'].next_by_code('resource'),
                          readonly=True)

    validate_url = fields.Boolean(defaul=False)
    validate_video = fields.Boolean(defaul=False)
    validate_quiz = fields.Boolean(defaul=False)

    @api.onchange('resource_type')
    def validate_url(self):
        if self.resource_type == 'url':
            self.validate_url = True
            self.validate_video = False
            self.validate_quiz = False
        elif self.resource_type == 'video':
            self.validate_url = False
            self.validate_video = True
            self.validate_quiz = False
        elif self.resource_type == 'quiz':
            self.validate_url = False
            self.validate_video = False
            self.validate_quiz = True
        else:
            self.validate_url = False
            self.validate_video = False
            self.validate_quiz = False
