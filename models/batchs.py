from odoo import models, fields, api


class Batches(models.Model):
    _name = 'learning.batches'

    name = fields.Char('Batch Name')
    code = fields.Char('Batch Code')
    disc = fields.Char('Course Description')
    users = fields.Integer('Number of Users', compute='_compute_users')
    user_count = fields.Integer(compute='_compute_users')
    trainee = fields.Many2many('res.partner', string='Users')

    @api.one
    def _compute_users(self):
        for partner in self:
            # operator = 'child_of' if partner.is_company else '='
            partner.user_count = self.env['res.partner'].search_count(
                [('batch', '=', self.id)])

    @api.multi
    def show_batch_users(self):
        return {
            'name': ('User'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'res.partner',  # model name ?yes true ok
            'target': 'current',
            'type': 'ir.actions.act_window',
            'context': {'default_batch': self.id},
            'domain': [('batch', '=', self.id)]
        }


class Badges(models.Model):
    _name = 'learning.badges'

    name = fields.Char('Badge Name')
    code = fields.Char('Badge Code')
    disc = fields.Char('Course Description')
    user_count = fields.Integer(compute='_compute_users')
    users = fields.Integer('Number of Users', compute='_compute_users')
    trainee = fields.Many2many('res.partner', string='Users')

    @api.one
    def _compute_users(self):
        for partner in self:
            # operator = 'child_of' if partner.is_company else '='
            partner.user_count = self.env['res.partner'].search_count(
                [('badge', '=', self.id)])

    @api.multi
    def show_badge_users(self):
        return {
            'name': ('User'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'res.partner',  # model name ?yes true ok
            'target': 'current',
            'type': 'ir.actions.act_window',
            'context': {'default_badge': self.id},
            'domain': [('badge', '=', self.id)]
        }
