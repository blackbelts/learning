from odoo import models, fields, api


class Subjects(models.Model):
    _name = 'learning.subjects'

    name = fields.Char()
    number_of_courses = fields.Integer("Num Of Courses", compute="compute_courses")
    code = fields.Char('Code')
    disc = fields.Char('Description')
    courses = fields.One2many('learning.courses', 'subject', string='Courses')
    course_count = fields.Integer(compute='_compute_courses')

    @api.one
    @api.model
    def compute_courses(self):
        courses = len(self.env['learning.courses'].search([('subject', '=', self.id)]))
        self.number_of_courses = courses

    @api.one
    def default_category(self):
        for rec in self.courses:
            rec.subject = self.id

    @api.one
    def _compute_courses(self):
        for partner in self:
            # operator = 'child_of' if partner.is_company else '='
            partner.course_count = self.env['learning.courses'].search_count(
                [('subject', '=', self.id)])

    @api.multi
    def show_category_courses(self):
        return {
            'name': ('Courses'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'learning.courses',  # model name ?yes true ok
            'target': 'current',
            'type': 'ir.actions.act_window',
            'context': {'default_subject': self.id},
            'domain': [('subject', '=', self.id)]
        }
