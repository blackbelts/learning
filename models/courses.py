from odoo import models, fields, api


class Courses(models.Model):
    _name = 'learning.courses'

    name = fields.Char('Course Name')
    code = fields.Char('Course Code')
    disc = fields.Char('Course Description')
    # teacher = fields.Many2one('res.partner', string='Teacher')
    subject = fields.Many2one('learning.subjects', string='Category')
    number_of_lessons = fields.Integer("Num Of Lessons", compute="_compute_lessons")
    state = fields.Selection([('draft', 'Draft'),
                              ('approved', 'Approved')],
                             'Status', required=True, default='draft')
    lesson_count = fields.Integer(compute='_compute_lessons')
    lessons = fields.One2many('learning.lessons', 'course', string='Lessons')

    # total_grades = fields.Integer("Grade")

    # @api.one
    # @api.model
    # def compute_Lessons(self):
    #     lessons = len(self.env['learning.lessons'].search([('course', '=', self.id)]))
    #     self.number_of_lessons = lessons
    @api.one
    def default_course(self):
        for rec in self.lessons:
            rec.course = self.id

    @api.one
    def _compute_lessons(self):
        for partner in self:
            # operator = 'child_of' if partner.is_company else '='
            partner.lesson_count = self.env['learning.lessons'].search_count(
                [('course', '=', self.id)])

    @api.multi
    def show_course_lessons(self):
        return {
            'name': ('Lessons'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'learning.lessons',  # model name ?yes true ok
            'target': 'current',
            'type': 'ir.actions.act_window',
            'context': {'default_course': self.id},
            'domain': [('course', '=', self.id)]
        }
