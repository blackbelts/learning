from odoo import models, fields, api


class CourseEnrollment(models.Model):
    _name = 'course.enrollment'

    code = fields.Char('Course Code')
    user_code = fields.Char('User Code')

    course = fields.Many2one('learning.courses', string='Course')
    user = fields.Many2one('res.user', string='User')
    completion = fields.Float('Completion%')
    total_score = fields.Float('Total Score')


class LessonEnrollment(models.Model):
    _name = 'lesson.enrollment'

    code = fields.Char('Lesson Code')

    course_enrollment = fields.Many2one('course.enrollment', string='Course Enrollment')
    completion = fields.Float('Completion%')


class LessonResourceEnrollment(models.Model):
    _name = 'lesson.resource.enrollment'

    code = fields.Char('Lesson Resource Code')
    lesson = fields.Many2one('learning.lessons', string='Lesson')
    completed = fields.Boolean('Completed')
