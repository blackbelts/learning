from odoo import models, fields, api


class Lessons(models.Model):
    _name = 'learning.quiz'

    name = fields.Char('Quiz Name')
    question_ids = fields.One2many('learning.question', 'quiz_id', string='Questions', copy=True)
    lesson_id = fields.Many2one('learning.lessons')
    trails = fields.Integer('Quiz Max Trails')
    time = fields.Integer('Quiz Time in minutes')
    marks = fields.Float('Quiz Total Marks')
    questions = fields.Integer('Quiz Number of Questions')


class Questions(models.Model):
    _name = 'learning.question'

    quiz_id = fields.Many2one('learning.quiz', string='Quiz',
                              ondelete='cascade', required=True, default=lambda self: self.env.context.get('quiz_id'))
    question = fields.Char('Question Code', required=True, translate=True)
    choice = fields.One2many('learning.answers', 'question_id', ondelete='cascade',
                             string='Choice')
    mark = fields.Float('Quiz Question Mark')


class QuestionsBank(models.Model):
    _name = 'learning.question.bank'

    code = fields.Char('Q Code')
    name = fields.Char('Q Title')
    header = fields.Char('Q Header')
    quiz_id = fields.Many2one('learning.quiz', string='Quiz',
                              ondelete='cascade', required=True, default=lambda self: self.env.context.get('quiz_id'))
    level = fields.Selection([('low', 'LOW'),
                              ('medium', 'MEDIUM'), ('high', 'HIGH')],
                             'Q Complexity Level', required=True)
    q_type = fields.Selection([('t/f', 'True Or False'),
                               ('msq-1-answer', 'MSQ 1 Answer'), ('msq-m-answers', 'MSQ Many Answers')],
                              'Q Type', required=True)
    question = fields.Char('Question Code', required=True, translate=True)

    mark = fields.Float('Q Default Mark')


class Answers(models.Model):
    _name = 'learning.answers'

    value = fields.Char('Suggested value', translate=True, required=True)
    quizz_mark = fields.Float('Score for this choice')
    question_id = fields.Many2one('learning.question')
