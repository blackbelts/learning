from odoo import models, fields, api


class inhertResPartner(models.Model):
    _inherit = 'res.partner'
    batch_count = fields.Integer(compute='_compute_batches')
    badge_count = fields.Integer(compute='_compute_badges')
    batch = fields.Many2many('learning.batches', string='User Batch')
    badge = fields.Many2many('learning.badges', string='User Badge')
    is_trainee = fields.Boolean('Trainee')

    @api.one
    def _compute_batches(self):
        if self.is_trainee == 1:
            for partner in self:
                # operator = 'child_of' if partner.is_company else '='
                partner.batch_count = self.env['learning.batches'].search_count(
                    [('trainee', '=', self.id)])

    @api.multi
    def show_partner_batches(self):
        if self.is_trainee == 1:
            return {
                'name': ('Batch'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'learning.batches',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_trainee': self.id},
                'domain': [('trainee', '=', self.id)]
            }

    @api.one
    def _compute_badges(self):
        if self.is_trainee == 1:
            for partner in self:
                # operator = 'child_of' if partner.is_company else '='
                partner.badge_count = self.env['learning.badges'].search_count(
                    [('trainee', '=', self.id)])

    @api.multi
    def show_partner_badges(self):
        if self.is_trainee == 1:
            return {
                'name': ('Batch'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'learning.badges',  # model name ?yes true ok
                'target': 'current',
                'type': 'ir.actions.act_window',
                'context': {'default_trainee': self.id},
                'domain': [('trainee', '=', self.id)]
            }
